package com.chokchaiduemchok.app1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView


class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btn = findViewById<Button>(R.id.buttonHello)
        val send_text = findViewById<TextView>(R.id.textName)

//        btn.setOnClickListener {
//            val intent = Intent(this,HelloActivity::class.java)
//            startActivity(intent)
//        }
            btn.setOnClickListener {
            val str = send_text.text.toString()
            val intent = Intent(applicationContext, HelloActivity::class.java)
            intent.putExtra("message_key", str)
            startActivity(intent)
        }






    }
}