package com.chokchaiduemchok.app1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class HelloActivity : AppCompatActivity() {

    lateinit var receiver_msg: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hello)

        val actionBar = supportActionBar

        receiver_msg = findViewById(R.id.received_value_id)
        val str = intent.getStringExtra("message_key")
        receiver_msg.text = str






    }
}